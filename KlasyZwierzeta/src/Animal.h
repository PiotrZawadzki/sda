/*
 * Animal.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef ANIMAL_H_
#define ANIMAL_H_
#include <string>

class Animal {
public:
	Animal(std::string givenName);
	virtual void speak();
	std::string giveAName(){return name;}

private:
	virtual void giveASound();
	std::string name;
};

#endif /* ANIMAL_H_ */
