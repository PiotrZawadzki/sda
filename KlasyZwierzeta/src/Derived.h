/*
 * Derived.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef DERIVED_H_
#define DERIVED_H_

#include "Base.h"

class Derived: public Base {
public:
	Derived();
	using Base::f;
	void f(double);
};

#endif /* DERIVED_H_ */
