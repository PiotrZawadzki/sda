/*
 * Cat.h
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef CAT_H_
#define CAT_H_
#include <string>
#include "Animal.h"

class Cat: public Animal {
public:
	Cat(std::string givenName);
	void giveASound();
};

#endif /* CAT_H_ */
