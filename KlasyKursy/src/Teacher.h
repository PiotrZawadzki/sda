/*
 * Teacher.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef TEACHER_H_
#define TEACHER_H_
#include <string>
#include "Person.h"

class Teacher: public Person {
public:
	Teacher();
	Teacher(std::string name, std::string surname, std::string email, int age);
};

#endif /* TEACHER_H_ */
