/*
 * Person.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */
//class Course
#ifndef PERSON_H_
#define PERSON_H_
#include <string>
class Course;
class Person {
public:
	Person();
	Person(std::string name, std::string surname, std::string email, int age);
	void describe();
	void showCourses();
	void addCourse(Course* course);

	int getAge() const {return age;}
	void setAge(int age) {this->age = age;}

	const std::string& getEmail() const {return email;}
	void setEmail(const std::string& email) {this->email = email;}

	const std::string& getName() const {return name;}
	void setName(const std::string& name) {this->name = name;}

	const std::string& getSurname() const {return surname;}
	void setSurname(const std::string& surname) {this->surname = surname;}

private:
	std::string name, surname, email;
	int age, numCourses;
	Course* courseList[20];


};

#endif /* PERSON_H_ */
