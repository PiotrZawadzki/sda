/*
 * Date.h
 *
 *  Created on: 06.04.2017
 *      Author: RENT
 */

#ifndef DATE_H_
#define DATE_H_

class Date {
public:
	Date();
	Date(int day, int month, int year);

private:
	int day, month, year;
};

#endif /* DATE_H_ */
