/*
 * DataBase.h
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */

#ifndef DATABASE_H_
#define DATABASE_H_
#include "Room.h"
#include "Person.h"
#include "Teacher.h"
#include "Student.h"
#include "Course.h"
class DataBase {
public:
	DataBase();
	Room rooms[3];
	Teacher teachers[5];
	Student students[20];
	Course courses[5];
	Date startDate;
	Date endDate;






};

#endif /* DATABASE_H_ */
