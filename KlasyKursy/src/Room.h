/*
 * Room.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef ROOM_H_
#define ROOM_H_
#include <string>
#include <iostream>
class Course;
class Room  {
public:
	Room();
	Room(int numRoom, int capacity, int building);
	void describe();
	void addCourse(Course* course);
	void coursesInRoom();

	int getBuilding() const {return building;}
	void setBuilding(char building) {this->building = building;}
	int getCapacity() const {return capacity;}
	void setCapacity(int capacity) {this->capacity = capacity;}
	int getNumRoom() const {return numRoom;}
	void setNumRoom(int numRoom) {this->numRoom = numRoom;}

private:

	int numRoom, capacity, numCoursesInRoom;
	int building;
	Course* courseList[20];

};

#endif /* ROOM_H_ */
