//============================================================================
// Name        : KlasyKursy.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>

#include "Room.h"
#include "Person.h"
#include "Teacher.h"
#include "Student.h"
#include "Course.h"
using namespace std;

int main() {

	Room room1(1,30,1);
	Room room2(2,25,1);
	Room room3(3,30,2);

	Date sdate1(15, 12, 2005);
	Date edate1(20, 1,2006);

	Teacher teacher1("Jan", "Kowalski", "kowalski@gmail.com", 70);
	Teacher teacher2("Pawel","Nowak","nowak@gmail.com", 55);

	Student student1 ("Karol","Malinowski","elo@gmail.com", 24);
	Student student2 ("Piotr","Herbaczewski","piotr@gmail.com", 27);
	Student student3 ("Maciej","Wach","wachu@gmail.com", 22);

	Course kurs1 (&room1,"Filozofia", "Wielki kurs filozofii",sdate1,edate1,30);

	kurs1.addTeacher(&teacher1);
	kurs1.addStudent(&student1);
	kurs1.showStudents();
	kurs1.showTeachers();
	kurs1.addStudent(&student2);
	kurs1.showStudents();
	student1.describe();
	teacher1.describe();
	room1.describe();

	return 0;
}

