/*
 * Person.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Person.h"
#include <string>
#include <iostream>
#include "Course.h"
Person::Person(){}
Person::Person(std::string name, std::string surname, std::string email, int age) {
	this->name=name;
	this->age=age;
	this->email=email;
	this->surname=surname;
	numCourses=0;
}
void Person::addCourse(Course* course) {
	courseList[numCourses] = course;
	numCourses++;
}
void Person::showCourses(){
	for (int i = 0; i < numCourses; i++) {
			std::cout << i + 1 << ". " << courseList[i]->getTopic() << std::endl;
		};
}
void Person::describe(){
	std::cout<<"**************************"<<std::endl;
	std::cout<<"Imie: "<<name<<std::endl;
	std::cout<<"Nazwisko: "<<surname<<std::endl;
	std::cout<<"Wiek: "<<age<<std::endl;
	std::cout<<"Adres e-mail: "<<email<<std::endl;
	showCourses();

}
