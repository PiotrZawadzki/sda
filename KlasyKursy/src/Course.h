/*
 * Course.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef COURSE_H_
#define COURSE_H_
#include <string>
#include "Date.h"
#include "Student.h"
#include "Teacher.h"
#include "Room.h"
#include "Person.h"
#include <iostream>

class Course {
public:
	Course(Room* room,std::string topic,std::string description, Date startDate,Date endDate, int maxPlace);
	void addStudent(Student* student);
	void showStudents();
	void showTeachers();
	void addTeacher(Teacher* teacher);

	const std::string& getDescription() const {return description;}
	void setDescription(const std::string& description) {this->description = description;}
	const Date& getEndDate() const {return endDate;}
	void setEndDate(const Date& endDate) {this->endDate = endDate;}
	int getFreePlace() const {return freePlace;}
	void setFreePlace(int freePlace) {this->freePlace = freePlace;}
	int getMaxPlace() const {return maxPlace;}
	void setMaxPlace(int maxPlace) {this->maxPlace = maxPlace;}
	const Date& getStartDate() const {return startDate;}
	void setStartDate(const Date& startDate) {this->startDate = startDate;}
	const std::string& getTopic() const {return topic;}
	void setTopic(const std::string& topic) {this->topic = topic;}

private:

	Date startDate;
	Date endDate;
	Room* room;
	Teacher* teachers[5];
	Student* students[20];
	int maxPlace, freePlace;
	std::string topic;
	std::string description;
	int numStudents, numTeachers;

};

#endif /* COURSE_H_ */
