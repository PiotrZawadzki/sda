/*
 * DataBase.cpp
 *
 *  Created on: 07.04.2017
 *      Author: RENT
 */

#include "DataBase.h"
#include "Room.h"
#include "Person.h"
#include "Teacher.h"
#include "Student.h"
#include "Course.h"
DataBase::DataBase() {

	rooms[0]=Room(1,20,1);
	rooms[1]=Room(2,25,1);
	rooms[2]=Room(1,24,2);

	teachers[0]=Teacher("Stanislaw","Nowak","stas@gmail.com",57);
	teachers[1]=Teacher("Kazimierz","Bubel","bubel@gmail.com",65);
	teachers[2]=Teacher("Eugeniusz","Motyka","motyk@gmail.com",47);
	teachers[3]=Teacher("Stanislaw","Kowalczyk","kowal@gmail.com",70);


	students[0]=Student("Waclaw","Baniak","glowka@gmail.com",17);
	students[1]=Student("Robert","Maslowski","masel@gmail.com",27);
	students[2]=Student("Mateusz","Herbaczewski","herbus@gmail.com",57);
	students[3]=Student("Piotr","Gietek","giecio@gmail.com",24);
	students[4]=Student("Bartosz","Budryk","budrys@gmail.com",25);

	Date startDate(15,12, 2017);
	Date endDate(14,02,2018);

	courses[0]=Course(&rooms[0],"Filozofia", "Wielki kurs filozofii",startDate,endDate,30);
	courses[1]=Course(&rooms[1],"Programowanie", "Wielki kurs programowania",startDate,endDate,25);
	courses[2]=Course(&rooms[2],"Matematyka", "Wielki kurs matematyki",startDate,endDate,20);




}

