/*
 * Student.h
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef STUDENT_H_
#define STUDENT_H_
#include <string>

#include "Person.h"

class Student: public Person {
public:
	Student();
	Student(std::string name, std::string surname, std::string email, int age);

};

#endif /* STUDENT_H_ */
