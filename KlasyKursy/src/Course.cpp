/*
 * Course.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "Course.h"
#include "Date.h"
#include "Student.h"
#include "Teacher.h"
#include "Room.h"
#include "Person.h"
#include <string>
#include <iostream>

Course::Course(Room* room,std::string topic,std::string description, Date startDate,Date endDate, int maxPlace) {
	this->topic=topic;
	this->description=description;
	this->startDate=startDate;
	this->endDate=endDate;
	this->room=room;
	freePlace=maxPlace;
	numStudents=0;
	numTeachers=0;
	room->addCourse(this);
	std::cout<<topic<<std::endl;
	std::cout<<description<<std::endl;
}
void Course::addStudent(Student* student){
	if(numStudents<room->getCapacity()){
	students[numStudents]=student;
	numStudents++;
	freePlace--;
	student->addCourse(this);
	}
	else {
		std::cout<<"Nie ma juz miejsca na kursie"<<std::endl;
	}
}
void Course::showStudents(){
	std::cout<<"Zapisani studenci na kurs: "<<std::endl;
	for (int i = 0; i<numStudents; i++){
		std::cout << i + 1 << ". " << students[i]->getName() <<" "<<students[i]->getSurname()<< std::endl;
	}
}
void Course::showTeachers(){
	for (int i = 0; i<numTeachers; i++){
		std::cout<<"Prowadzacy kurs: "<<std::endl;
			std::cout << i + 1 << ". " << teachers[i]->getName() <<" "<<teachers[i]->getSurname()<< std::endl;
}
}
void Course::addTeacher (Teacher* teacher) {
		if(numTeachers<5){
		teachers[numTeachers]=teacher;
		numTeachers++;
		teacher->addCourse(this);
		}
		else {
			std::cout<<"Nie ma juz miejsca na kursie"<<std::endl;
		}
	}
