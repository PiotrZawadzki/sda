/*
 * DriveSystem.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef DRIVESYSTEM_H_
#define DRIVESYSTEM_H_

class DriveSystem {
public:
	void shiftFrontGearUp();
	void shiftFrontGearDown();
	void shiftBackGearUp();
	void shiftBackGearDown();
	DriveSystem();
	DriveSystem();

private:

	int frontGearMax;
	int backGearMax;
	int frontGear;
	int backGear;
};

#endif /* DRIVESYSTEM_H_ */
