/*
 * Lights.cpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#include "Lights.h"

Lights::Lights() {
	isFrontLightOn=false;
	isRearLightOn=false;

}
void Lights::frontLightOn(){
	isFrontLightOn=true;
}
void Lights::frontLightOff() {
	isFrontLightOn=false;
}
void Lights::rearLightOff(){
	isRearLightOn=false;
}
void Lights::rearLightOn(){
	isRearLightOn=true;
}
