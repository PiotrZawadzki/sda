/*
 * Frame.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef FRAME_H_
#define FRAME_H_

class Frame {
public:
	enum frameType{hardtail, softail};
	enum frameSize{S,M,L,XL};
	enum frameMaterial{aluminium, steel, carbon};

	Frame();
	Frame(frameType ntype, frameSize nsize, frameMaterial nmaterial);

private:
	frameType type;
	frameSize size;
	frameMaterial material;
};
#endif /* FRAME_H_ */
