/*
 * Wheel.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef WHEEL_H_
#define WHEEL_H_

class Wheel {

private:
	int frontWheelSize;
	int rearWheelSize;

public:
	Wheel(int size1=17, int size2=18);
};

#endif /* WHEEL_H_ */
