/*
 * Lights.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef LIGHTS_H_
#define LIGHTS_H_

class Lights {
public:
	Lights();
	void frontLightOn();
	void frontLightOff();
	void rearLightOff();
	void rearLightOn();


private:
	bool isFrontLightOn;
	bool isRearLightOn;
};

#endif /* LIGHTS_H_ */
