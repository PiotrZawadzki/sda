/*
 * Bike.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef BIKE_H_
#define BIKE_H_
#include "Frame.h"
#include "Wheel.h"
#include "Lights.h"
#include "DriveSystem.h"
class Bike {
public:
	Bike();
private:
	Frame frame;
	Wheel frontWheel;
	Wheel rearWheel;
	Lights frontLight;
	Lights rearLight;
	DriveSystem chain;
	DriveSystem frontGears;
	DriveSystem rearGears;
	DriveSystem derailleur;
	DriveSystem pedals;
};

#endif /* BIKE_H_ */
//rower:
/*

kola2x klasa
rama klasa
hamulec x2
amortyzator
kierownica
siodelko
oswietlenie(przod tyl odblaski) klasa
uklad napedowy: klasa
lancuch
biegi przod
biegi tyl
przerzutka
pedaly*/
