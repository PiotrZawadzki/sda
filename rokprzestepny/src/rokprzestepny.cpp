//============================================================================
// Name        : rokprzestepny.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Wprowadz rok i sprawdz czy jest przestepny: " << endl;
	int rok;
	cin >> rok;
	if (((rok % 4 == 0) && (rok % 100 != 0)) || (rok % 400 == 0)) {
		cout << "Podany rok jest przestepny!" << endl;
	} else
		cout << "Podany rok nie jest przestepny" << endl;
	return 0;
}
