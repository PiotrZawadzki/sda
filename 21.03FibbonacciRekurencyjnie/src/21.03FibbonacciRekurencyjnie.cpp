//============================================================================
// Name        : 03FibbonacciRekurencyjnie.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int fib(int n) {
	if (n == 0) {
		return 0;
	} else if (n == 1) {
		return 1;
	} else {

		return (fib(n - 1) + fib(n - 2));

	}
}
int main() {
	cout << "Wprowadz numer ciagu fibbonaciego ktory chcesz wyswietlic: ";
	int a;
	cin >> a;
	cout << fib(a) << endl; // prints !!!Hello World!!!
	return 0;
}
