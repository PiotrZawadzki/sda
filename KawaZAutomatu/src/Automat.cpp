/*
 * Automat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "Automat.h"
#include <cmath>

Automat::Automat() {
	productsList[0].name = "Snickers";
	productsList[0].price = 2.5;
	productsList[1].name = "Kawa czarna";
	productsList[1].price = 2;
	productsList[2].name = "Kawa biala";
	productsList[2].price = 3;
	productsList[3].name = "Herbata";
	productsList[3].price = 1.5;
	productsList[4].name = "Mars";
	productsList[4].price = 2.5;
	productsList[5].name = "Woda niegazowana";
	productsList[5].price = 1.5;
	arraySize=6;

}

Automat::~Automat() {
	// TODO Auto-generated destructor stub
}
void Automat::showMenu() {
	std::cout << "Produkty do wyboru:" << std::endl;
	for (int i = 0; i <arraySize; i++) {
		std::cout << i + 1 << "." << productsList[i].name << ", cena: "
				<< productsList[i].price << " zl" << std::endl;
	}
}
bool Automat::chooseProduct() {
	while (true) {
		std::cout << "Wybierz numer produktu: ";
		std::cin >> choice;
		if (choice > 0 && choice <= maxSpace) {
			ptrPrice = &productsList[choice - 1].price;
			ptrName = &productsList[choice - 1].name;
			std::cout << "Wybrales : " << productsList[choice - 1].name
					<< ", wrzuc " << productsList[choice - 1].price << " zl"
					<< std::endl;
			return true;
		} else {
			std::cout << "Niepoprawny numer ! ";
		}
	}
}
bool Automat::pay() {

	std::cin >> money;
	if (money == *ptrPrice) {
		std::cout << *ptrName << " do odebrania! " << std::endl;
	}
	if (money > *ptrPrice) {
			std::cout << *ptrName << " do odebrania! " << std::endl;
			std::cout << "Prosze odebrac reszte : " << money - *ptrPrice<<" zl"<< std::endl;
		}

	if ((money < *ptrPrice) && (money > 0)) {
		change = *ptrPrice - money;
		while (change>0) {
			std::cout << "Musisz doplacic: " << change << std::endl;
			std::cin>>cos;
			change-=cos;
		}
		std::cout << *ptrName << " do odebrania! " << std::endl;
		if (change<0) {
			std::cout << "Prosze odebrac reszte : " << std::abs(change)<<" zl"<< std::endl;
		}
	}
	removeProduct();
}
void Automat::removeProduct(){
	Product temp;
	for (int i=choice;i <arraySize; i++) {
		temp=productsList[i];
		productsList[i-1]=temp;
	}
	arraySize--;
}
