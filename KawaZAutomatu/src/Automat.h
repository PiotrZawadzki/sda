/*
 * Automat.h
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef AUTOMAT_H_
#define AUTOMAT_H_
#include <string>
const int maxSpace = 10;

struct Product {
    std::string name;
    float price;
};


class Automat {
public:
    Automat();
    virtual ~Automat();
    void showMenu();
    bool chooseProduct();
    bool pay();
    void removeProduct();
    Product productsList[maxSpace];
private:

    int choice;
    float money;
    float* ptrPrice;
    std::string* ptrName;
    float change;
    float cos;
    int arraySize;

};

#endif /* AUTOMAT_H_ */
