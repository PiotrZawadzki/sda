/*
 * Lista.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#include "Lista.h"
#include <string>
#include <cstdlib>
#include <iostream>

Lista::Lista() {
	pierwsza = 0;

}

Lista::~Lista() {
	// TODO Auto-generated destructor stub
}

Lista::osoba::osoba() {
	nastepna = 0;
}

void Lista::dodaj_osobe(std::string imie, std::string nazwisko, int wiek) {

	osoba *nowa = new osoba;
	nowa->imie = imie;
	nowa->nazwisko = nazwisko;
	nowa->wiek = wiek;
	if (pierwsza == 0){
		pierwsza = nowa;
	}
	else {
		osoba * temp= pierwsza;
		while(temp->nastepna){
			temp = temp->nastepna;
		}
		temp->nastepna = nowa;
		nowa->nastepna = 0;
	}
}

void Lista::wyswietl_liste() {

	osoba* temp = pierwsza;
	while(temp){
		std::cout<<"imie: "<<temp->imie<<"nazwisko: "<<temp->nazwisko<<"wiek: "<<temp->wiek<<std::endl;
		temp = temp->nastepna;
	}


}

void Lista::usun_osobe(int nr) {

	// jezeli to pierwszy element listy
	    if (nr==1)
	    {
	        osoba *temp = pierwsza;
	        pierwsza = temp->nastepna; //poczatek listy, 1 el. wskazuje na 2 el. itd..
	    }

	    // je�eli nie jest to pierwszy element
	    if (nr>=2)
	    {
	        int j = 1;

	        // do usuniecia srodkowego elemetnu potrzebujemy wskaznika na osobe n-1
	        // wskaznik *temp bedzie wskaznikiem na osobe poprzedzajaca osobe usuwana
	        osoba *temp = pierwsza;

	        while (temp)
	        {
	            // sprawdzamy czy wskaznik jest na osobie n-1 niz usuwana
	            if ((j+1)==nr) break;

	            // jezeli nie to przewijamy petle do przodu
	            temp = temp->nastepna;
	            j++;
	        }

	        // wskaznik *temp wskazuje teraz na osobe n-1
	        // nadpisujemy wkaznik osoby n na osobe n+1
	        // bezpowrotnie tracimy osobe n-ta

	        // dodatkowo sprawdzamy czy aby nie jest to ostatni element
	        // wtedy nalezy wyzerowac ostatni wskaznik
	        if (temp->nastepna->nastepna==0)
	            temp->nastepna = 0;

	        // jezeli nie byl to ostatni element
	        else
	            temp->nastepna = temp->nastepna->nastepna;
	    }
}
