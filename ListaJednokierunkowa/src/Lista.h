/*
 * Lista.h
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef LISTA_H_
#define LISTA_H_
#include <string>
#include <cstdlib>
class Lista {
public:
	Lista();
	virtual ~Lista();


	struct osoba {
		std::string imie;
		std::string nazwisko;
		int wiek;
		osoba();
		osoba *nastepna;
	};
	osoba *pierwsza;
	void dodaj_osobe(std::string imie,std::string nazwisko, int wiek);
	void usun_osobe(int nr);
	void wyswietl_liste();

};

#endif /* LISTA_H_ */
