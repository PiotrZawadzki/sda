//============================================================================
// Name        : miejscazerowe.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

int main() {
	double a;
	double b;
	double c;
	double xp;
	double xd;
	cout << "Rownanie kwadratowe y = ax^2 + bx + c" << endl;
	cout << "Wprowadz wspolczynniki a, b i c:";
	cin >> a >> b >> c;

	double delta = ((b * b) - 4 * a * c);
	if (delta > 0) {
		xp = ((-b - sqrt(delta)) / (2 * a));
		xd = ((-b + sqrt(delta)) / (2 * a));
		cout << "Delta wynosi: " << delta << endl;
		cout << "Miejsce zerowe x1 = " << xp << endl;
		cout << "Miejsce zerowe x2 = " << xd << endl;
	} else if (delta == 0) {
		xp = ((-b - sqrt(delta)) / (2 * a));
		cout << "Miejsce zerowe wynosi : " << xp << endl;
	}

	else {
		cout << "nie ma miejsc zerowych" << endl;
	}
	return 0;
}
