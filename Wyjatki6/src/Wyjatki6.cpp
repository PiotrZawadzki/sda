//============================================================================
// Name        : Wyjatki6.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
#include <cmath>
#include <typeinfo>
#define PI 3.14159265
using namespace std;

namespace math {

class MathWithExceptions {
public:
	class DomainException: public exception {
	public:

	//	DomainException(string value){

	//	}
	//	virtual ~DomainException();
		virtual const char* what() const throw () {
			return "Domain exception ";
		}
	//	const string exception_type;
	};
	class RangeException: public exception {
	public:
		virtual const char* what() const throw () {
			return "Range exception ";
		}
	};
	MathWithExceptions();
	virtual ~MathWithExceptions();


	static double funLog(int x) {
		double result = log(x);
		if (errno == EDOM) {
			DomainException e;
			throw e;
		}
		if (errno == ERANGE) {
			RangeException e;
			throw e;
		}
		return result;
	}
	static double funPow(int x, int y) {
		double result = pow(x, y);
		if (errno == EDOM) {
			DomainException e;
			throw e;
		}
		if (errno == ERANGE) {
			RangeException e;
			throw e;
		}
		return result;
	}
	static double funCos(double x) {
		int result = cos(x * PI / 180);
		if (errno == EDOM) {
			DomainException e;
			throw e;
		}
		if (errno == ERANGE) {
			RangeException e;
			throw e;
		}
		return result;
	}
	static double funSin(double x) {
		int result = sin(x * PI / 180);
		if (errno == EDOM) {
			DomainException e;
			throw e;
		}
		if (errno == ERANGE) {
			RangeException e;
			throw e;
		}
		return result;
	}
	static double funTan(int x) {
		int result = tan(x * PI / 180);
		if (errno == EDOM) {
			DomainException e;
			throw e;
		}
		if (errno == ERANGE) {
			RangeException e;
			throw e;
		}
		return result;
	}

};

}
int main() {

	try{
		double a = math::MathWithExceptions::funLog(-1);

	//	double b = math::MathWithExceptions::funPow(5, 7);
		//double c = math::MathWithExceptions::funCos(45);
	//	double d = math::MathWithExceptions::funSin(78);
		//double e = math::MathWithExceptions::funTan(45);

	}
	catch(exception &e){
		std::cout<<"Exception: "<<e.what()<<std::endl;
	}
	catch(...){
			std::cout<<"unrecognized exception"<<std::endl;
		}

	return 0;
}
