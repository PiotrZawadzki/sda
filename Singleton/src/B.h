/*
 * B.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef B_H_
#define B_H_

class B {

public:
	virtual ~B();

	static B* getInstance();
private:
	B();
	B(const B&);
	static B* instance;

};

#endif /* B_H_ */
