/*
 * ID.h
 *
 *  Created on: 11.05.2017
 *      Author: RENT
 */

#ifndef ID_H_
#define ID_H_

class ID {
public:
	ID();
	virtual ~ID();
	char createID();

private:
	static int counter;

};

#endif /* ID_H_ */
