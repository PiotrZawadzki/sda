/*
 * LogWriter.h
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef LOGWRITER_H_
#define LOGWRITER_H_
#include <fstream>
#include <string>

class LogWriter {
public:
	LogWriter();
	virtual ~LogWriter();
	enum logType {Fatal,Error,Warning,Info,Debug,Trace};

	void logMessage(std::string message,logType messageType);
	void setLoggingLevel(logType logLevel);
	std::string enumToString(logType logLevel);

private:
	enum logType logLevel;
	std::string message;
	std::fstream logFile;

};

#endif /* LOGWRITER_H_ */
