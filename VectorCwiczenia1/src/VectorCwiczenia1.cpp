//============================================================================
// Name        : VectorCwiczenia1.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
using namespace std;

int main() {

	int myInts[] = { 17, 24, 35, 88, 10, 5 };
	std::vector<int> myVector(myInts, myInts + sizeof(myInts) / sizeof(int));
	int min = myVector.at(0);
	int max = myVector.at(0);
	for (int unsigned i = 0; i < myVector.size(); i++) {
		if (min > myVector[i]) {
			min = myVector[i];
		}
		if (max < myVector[i]) {
			max = myVector[i];
		}
	}
	std::cout << "Min value: " << min << " Max value: " << max << std::endl;
	int total = 0;
	for (int unsigned i = 0; i < myVector.size(); i++) {
		total += myVector.at(i);
	}
	std::cout << "Sum of myVector " << total << std::endl;
	std::cout << "Diff next two elements: " << std::endl;
	for (int unsigned i = 1; i < myVector.size(); i++) {
		std::cout << i << "-" << i - 1 << ": ";
		std::cout << myVector.at(i) - myVector.at(i - 1) << std::endl;
	}
	std::cout << "Values of copy vector: ";
	std::vector<int> cpyVector;
	for (int unsigned i = 0; i < myVector.size(); i++) {
		cpyVector.push_back(myVector[i]);
	}
	for (int unsigned i = 0; i < myVector.size(); i++) {
		std::cout << cpyVector[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Adding value to vector: ";
	int x = 15;
	myVector.insert(myVector.begin() + 2, x);
	for (int unsigned i = 0; i < myVector.size(); i++) {
		std::cout << myVector.at(i) << " ";
	}
	std::cout << std::endl;
	std::cout << "Deleting value of vector: ";

	myVector.erase(myVector.begin() + 5);
	for (int unsigned i = 0; i < myVector.size(); i++) {
		std::cout << myVector.at(i) << " ";
	}
	return 0;
}
