//============================================================================
// Name        : 03WskCwiczenie.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
const int max_array_size = 5;

int main() {
    int t[max_array_size];
    cout << "Podaj 5 dowolnych liczb calkowitych" << endl;
    for(int i=0; i<max_array_size; ++i){
        cin >> t[i];
    }
    int *it=t;
    for(int i=0; i<max_array_size; ++i){
        cout << *it++ << " ";
    }

    return 0;

}
