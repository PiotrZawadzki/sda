/*
 * validDateTest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "functions.h"
#include "gtest.h"

TEST(validDateTest, Zero) {
	EXPECT_EQ(0, validDate(0,2,2000));
}
TEST(validDateTest, valid) {
	EXPECT_EQ(1, validDate(31,12,2000));
}
TEST(validDateTest, NonValid) {
	EXPECT_EQ(0, validDate(29,2,2001));
}
TEST(validDateTest, zeroMonth) {
	EXPECT_EQ(0, validDate(20,-5,2000));
}

