/*
 * nwdTest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "functions.h"
#include "gtest.h"

TEST(nwdTest, ZeroZero) {
	EXPECT_EQ(0, nwd(0,0));
}
TEST(nwdTest, ZeroOne) {
	EXPECT_EQ(1, nwd(0,1));
}
TEST(nwdTest, OneZero) {
	EXPECT_EQ(1, nwd(1,0));
}
TEST(nwdTest, TenFive) {
	EXPECT_EQ(5, nwd(10,5));
}
TEST(nwdTest, FiveTen) {
	EXPECT_EQ(5, nwd(5,10));
}
TEST(nwdTest, NegativePlus) {
	EXPECT_EQ(5, nwd(-5,10));
}
TEST(nwdTest, Negative) {
	EXPECT_EQ(5, nwd(-5,-10));
}
