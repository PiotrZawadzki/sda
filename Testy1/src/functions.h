/*
 * functions.h
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_
#include <iostream>

int silnia(int x);
int fib(int fibNumber);
int nwd(int num1, int num2);
int daysInMonth(int month);
bool validDate(int day, int month, int year);

#endif /* FUNCTIONS_H_ */
