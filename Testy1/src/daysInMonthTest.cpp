/*
 * daysInMonthTest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "functions.h"
#include "gtest.h"

TEST(daysInMonthTest, Zero) {
	EXPECT_EQ(0, daysInMonth(0));
}
TEST(daysInMonthTest, Negative) {
	EXPECT_EQ(0, daysInMonth(-5));
}
TEST(daysInMonthTest, TooMuch) {
	EXPECT_EQ(0, daysInMonth(13));
}
TEST(daysInMonthTest, December) {
	EXPECT_EQ(31, daysInMonth(12));
}
TEST(daysInMonthTest, Febuary) {
	EXPECT_EQ(28, daysInMonth(2));
}
TEST(daysInMonthTest, April) {
	EXPECT_EQ(30, daysInMonth(4));
}

