/*
 * silniaTest.cpp
 *
 *  Created on: 21.04.2017
 *      Author: RENT
 */

#include "functions.h"
#include "gtest.h"

TEST(silniaTest, Zero) {
	EXPECT_EQ(1, silnia(0));
}
TEST(silniaTest, Three) {
	EXPECT_EQ(6, silnia(3));
}
TEST(silniaTest, Four) {
	EXPECT_EQ(24, silnia(4));
}
TEST(silniaTest, NegativeFive) {
	EXPECT_EQ(-1, silnia(-5));
}
TEST(silniaTest, NegativeHundred) {
	EXPECT_EQ(-1, silnia(-100));
}
