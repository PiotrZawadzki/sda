/*
 * fibonacciTest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "functions.h"
#include "gtest.h"

TEST(fibTest, Zero) {
	EXPECT_EQ(0, fib(0));
}
TEST(fibTest, Three) {
	EXPECT_EQ(2, fib(3));
}
TEST(fibTest, Ten) {
	EXPECT_EQ(55, fib(10));
}
TEST(fibTest, Negative) {
	EXPECT_EQ(-1, fib(-10));
}
TEST(fibTest, One) {
	EXPECT_EQ(1, fib(1));
}
TEST(fibTest, Two) {
	EXPECT_EQ(1, fib(2));
}


