//============================================================================
// Name        : 03Cwicz1.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

const int max_array_size = 5;
void fun(int a);
// wypisuje liczby ktorych suma cyfr jest parzysta wykorzystujac funkcje
int main() {
	int t[max_array_size] = { 23, 12, 67, 22, 11 };

	cout << "Liczby ktorych suma cyfr jest parzysta to: " << endl;
	for (int i = 0; i < max_array_size; ++i) {

		fun(t[i]);
	}
	return 0;
}
void fun(int a) {
	int temp = a;
	int sum = 0;
	while (temp != 0) {
		sum += temp % 10;
		temp /= 10;
	}
	if (sum % 2 == 0) {
		cout << a << endl;
	}
}

