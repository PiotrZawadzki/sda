//============================================================================
// Name        : Wyjatki4.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <math.h>
#include <fstream>
#include <typeinfo>
#include <exception>
using namespace std;

class edomException : public exception {
 virtual const char* what() const throw() {
	return "EDOM exception";
}

};
class erangeException : public exception {
virtual const char* what() const throw(){
	return "ERANGE exception";
}

};

int myPow(int x, int y) {
	int a = pow(x, y);
	if(errno == EDOM){
		edomException e;
		throw e;
	}
	if(errno == ERANGE){
		erangeException e;
		throw e;
	}
	return a;
}

int myLog(int z) {
	int b = log(z);
	return b;
}
int main() {

	try {
		int z = myPow(10000, 1000000);
		//std::cout << myLog(2) << std::endl;
	} catch (exception& e) {
		cout<<"exception: "<<e.what();
	}

	return 0;
}
