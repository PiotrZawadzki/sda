//============================================================================
// Name        : CwiczeniaRekrutacyjne1.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int solution (int *A, int size_of_A){

	int min = A[0];
	for(int i = 0; i<size_of_A; i++){
		if(min>A[i]){
			min = A[i];
		}
	}
	return min;

}

int main() {


	int A[]= {1, 2, 3, 42, 1, -10};
	std::cout<<solution(A, 6);

	int B[]= {1, -2147483648, 3, 42, 2, 2147483648};
	int C[]= {-100, 0, 1, 42, -10, -10};
	int D[]= {1, -200, 3, 42, 1, -10};
	std::cout<<solution(B, 6);
	std::cout<<solution(C, 6);
	std::cout<<solution(D, 6);
	return 0;
}
