//============================================================================
// Name        : 2017cw6.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
cout<<"Tabliczka while: "<<endl;
	int i = 0;
	while (++i <= 10) {
		int j = 0;
		while (++j <= 10) {
			cout << i * j << "\t";

		}

		cout << " " << endl;
	}
cout<<"Tabliczka for: "<<endl;


	for(int i=1;i<=10;++i) {
			for(int j=1; j<=10;j++) {
				cout<<i*j<<" "<<"\t";
			}
			cout<<endl;
		}
	return 0;
}
