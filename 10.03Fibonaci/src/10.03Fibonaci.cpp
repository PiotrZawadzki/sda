//============================================================================
// Name        : 03Fibonaci.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
int fib(int fibNumber) {
		if (fibNumber == 0) {
			return 0;
		}
		if(fibNumber<3)
		    return 1;

		  return fib(fibNumber-2)+fib(fibNumber-1);
	}
int main() {

	std::cout<<fib(7);

	return 0;
}
