//============================================================================
// Name        : 03UnikalneWartosci.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
const int max = 6;
int main() {
	int tab[max] = {2, 2, 2, 4, 5, 6};
	int duplikaty=0;
	for (int i = 0; i < max; ++i) {
	    for (int j = 0; j < max; ++j) {

	        if ((tab[i] == tab[j]) && tab[i] != 0 && i != j) // && tab[j]!="0" )
	            tab[i] = 0;
	    }
	    if (tab[i] == 0)
	        ++duplikaty;
	}
	cout<<duplikaty;
	return 0;
}
