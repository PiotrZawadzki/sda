//============================================================================
// Name        : List1.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>

int main() {
	std::list<int> first;  //pusty
	std::list<int> second (4, 100);
	std::list<int> third (second.begin(),second.end());
	std::list<int> fourth (third);

	int myints[] = {16,2, 77, 29, 35, 75, 77, 16, 4};
	std::list<int> fifth (myints, myints + sizeof(myints)/sizeof(int));
	for(std::list<int>::iterator it = fifth.begin(); it !=fifth.end(); it++){
		std::cout<<*it<<" ";
		std::cout<< '\n';
	}
	std::cout<< '\n';
	fifth.sort();
	for(std::list<int>::iterator it = fifth.begin(); it !=fifth.end(); it++){
			std::cout<<*it<<" ";
		}
	std::cout<< '\n';
	fifth.unique();
	for(std::list<int>::iterator it = fifth.begin(); it !=fifth.end(); it++){
				std::cout<<*it<<" ";
			}

	std::cout<< '\n';
	std::cout<< '\n';
	std::list<int> sixth;
	for(int i = 0; i<5;i++){
		sixth.push_back(i);
	}
	std::list<int>::iterator i, i2, i3;
	i = sixth.begin();
	advance(i,2);
	i2 = i3 = fifth.begin();
	advance(i2,4);
	advance(i3,7);
	sixth.splice(i, fifth, i2, i3);
	for(std::list<int>::iterator it = fifth.begin(); it !=fifth.end(); it++){
					std::cout<<*it<<" ";

				}
	std::cout<< '\n';
	for(std::list<int>::iterator it = sixth.begin(); it !=sixth.end(); it++){
					std::cout<<*it<<" ";
				}

	return 0;
}
