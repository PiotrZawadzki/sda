//============================================================================
// Name        : CwiczenieEgz.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstring>
#include <cstdlib>
using namespace std;
bool isPalindrom(char word[]);

int main() {

	char slowo[100];
	cin >> slowo;
	std::cout << isPalindrom(slowo);

	return 0;
}

bool isPalindrom(char word[]) {

	for (int i = 0, j = strlen(word) - 1; i < j; i++, j--) {
		if (word[i] != word[j]){
			return false;
		}
		return true;
	}
}

