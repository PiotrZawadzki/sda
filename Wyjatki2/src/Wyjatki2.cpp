//============================================================================
// Name        : Wyjatki2.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <typeinfo>
#include <fstream>
#include <string>
using namespace std;

class Foo {
public:
	virtual ~Foo() {}
};

class Bar {
public:
	virtual ~Bar(){}
};

void displayException(exception& e){
	std::cout<<"exception: "<<e.what()<<endl;
}
void badException() throw(bad_exception) {
	bad_exception e;
	throw e; //bad exception
}

int main() {

	Bar b;
	Bar *c = 0;
	try{
		/*int *test = new  int[10000000000]; //bad alloc
		int *test1 = new (nothrow) int [10000000000]; // bez wyjatkow

		Foo &f = dynamic_cast<Foo&>(b); //bad cast
		badException(); // bad exception
		std::cout<<typeid(*c).name()<<std::endl;  //bad typeid*/
/*

		ifstream f("moj plik nie istnieje");
		f.open("moj plik nie istnieje");
*/

		/*string text;
		text.at(2);*/   // out of range
	}
	catch(bad_alloc& e){
		displayException(e);
	}
	catch(bad_cast& e){
		displayException(e);
	}
	catch(bad_exception& e){
		displayException(e);
	}
	catch(bad_typeid& e){
		displayException(e);
	}
	catch(ios_base::failure& e){
		displayException(e);
	}
	catch(out_of_range& e){
		displayException(e);
	}
	catch(logic_error& e){
		//invalid argument
		//domain error
		//length error
			displayException(e);
		}
	catch(...){
		std::cout<<"unrecognized exception"<<std::endl;
	}

	return 0;
}
