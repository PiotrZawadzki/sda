//============================================================================
// Name        : Wyjatki.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
void last() {

	std::cout << "Last start" << std::endl;
	std::cout<< "throwing int exception"<<std::endl;
	throw -1.5;
	std::cout << "Last end" << std::endl;


}
void third() {

	std::cout << "Third start" << std::endl;


			last();

			std::cout << "Third end" << std::endl;
}
void second() {

	std::cout << "Second start" << std::endl;
		try {
			third();
		}
		 catch (double) {
			std::cerr << "in second double exception" << std::endl;
		}
		std::cout << "Second end" << std::endl;
}

void first() {

	std::cout << "First start" << std::endl;
	try {
		second();
	} catch (int) {
		std::cerr << "in first int exception" << std::endl;
	} catch (double) {
		std::cerr << "in first double exception" << std::endl;
	}
	std::cout << "First end" << std::endl;
}
int main() {
	std::cout<<"Main start"<<std::endl;
	try {
		int x = 2;
		int y = 0;
		if (y == 0)
			throw "error division by 0";
		int d = x/y;
	} catch (const char* exception) {
		std::cerr<<"in main any exception catched"<<exception<<std::endl;

	}
	std::cout<<"main end"<<std::endl;
	return 0;
}
