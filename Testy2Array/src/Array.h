/*
 * Array.h
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
const int maxArraySize = 1000;

#ifndef ARRAY_H_
#define ARRAY_H_

class Array {
public:
	Array();
	virtual ~Array();
	void fillArray();
	int getArraySize();
	int getIndexValue(int index);
	void addLastElement(int element);
	void removeLastElement();
	void addElement(int index, int element);
	void removeIndex(int index);
	int getMax();
	int getMin();
	 float getArray()  {
		 float wsk = *array;
		 return wsk;
	}


private:
	float array[maxArraySize];
	int arraySize;

};

#endif /* ARRAY_H_ */
