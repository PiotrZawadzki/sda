/*
 * ArrayTest.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "gtest.h"
#include "Array.h"

class ArrayTesting : public testing::Test {

public:
	Array tabela;

};

TEST_F(ArrayTesting, tabSetTest){
	tabela.fillArray();
	EXPECT_EQ(0,tabela.getIndexValue(0));
	EXPECT_EQ(15,tabela.getIndexValue(15));
	EXPECT_EQ(0,tabela.getIndexValue(50));
}
TEST_F(ArrayTesting, tabPtrTest){

	tabela.fillArray();
	EXPECT_EQ(0,tabela.getArray());

}
TEST_F(ArrayTesting, tabRemoveLast){
	tabela.fillArray();

	tabela.removeLastElement();
	tabela.removeLastElement();
	EXPECT_EQ(0,tabela.getIndexValue(999));
	EXPECT_EQ(0,tabela.getIndexValue(49));
	EXPECT_EQ(0,tabela.getIndexValue(48));
}
TEST_F(ArrayTesting, tabAddLast){

	tabela.fillArray();
	tabela.addLastElement(10);
	tabela.addLastElement(12);
	EXPECT_EQ(10,tabela.getIndexValue(50));
	EXPECT_EQ(12,tabela.getIndexValue(51));
	EXPECT_EQ(0,tabela.getIndexValue(52));
	/*EXPECT_EQ(0,tabela.getIndexValue(3));
	EXPECT_EQ(4,tabela.getIndexValue(4));
	EXPECT_EQ(-1,tabela.getIndexValue(-5));
	EXPECT_EQ(-1,tabela.getIndexValue(1050));*/

}
TEST_F(ArrayTesting, tabArraySizeTest){


	EXPECT_EQ(0,tabela.getArraySize());
	tabela.fillArray();
	EXPECT_EQ(50,tabela.getArraySize());

}
TEST_F(ArrayTesting, tabAddElementTest){



	tabela.fillArray();
	EXPECT_EQ(50,tabela.getArraySize());
	tabela.addElement(10,35);
	EXPECT_EQ(35,tabela.getIndexValue(10));
	EXPECT_EQ(10,tabela.getIndexValue(11));
	EXPECT_EQ(51,tabela.getArraySize());
	EXPECT_EQ(11,tabela.getIndexValue(12));
	EXPECT_EQ(12,tabela.getIndexValue(13));
	EXPECT_EQ(49,tabela.getIndexValue(50));

}
TEST_F(ArrayTesting, tabremoveIndexTest){

	tabela.fillArray();
	tabela.removeIndex(34);
	EXPECT_EQ(35,tabela.getIndexValue(34));
	EXPECT_EQ(36,tabela.getIndexValue(35));
	EXPECT_EQ(49,tabela.getArraySize());

}
TEST_F(ArrayTesting, tabGetMaxTest){

	tabela.fillArray();

	EXPECT_EQ(49,tabela.getMax());
	tabela.addElement(10,58);
	EXPECT_EQ(58,tabela.getMax());
}
TEST_F(ArrayTesting, tabGetMinTest){

	tabela.fillArray();

	EXPECT_EQ(0,tabela.getMin());

}










