/*
 * Array.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "Array.h"

Array::Array() {
	arraySize = 0;

}

Array::~Array() {
	// TODO Auto-generated destructor stub
}

void Array::fillArray() {
		for(int i= 0; i <50; i++) {

			array[i]=i;
			arraySize++;
		}
	}
int Array::getArraySize() {
	return arraySize;
}
int Array::getIndexValue(int index){
	if (index > 0 && index < maxArraySize){
	return array[index];
	}
	if (index == 0) {
		return array[index];
	}
	else
		return -1;
}
void Array::addLastElement(int element){
	if (arraySize< maxArraySize) {
		array[arraySize]=element;
		arraySize++;
	}
}
void Array::removeLastElement(){
	if (arraySize>0){
		arraySize--;
		array[arraySize]=0;
	}
}
void Array::addElement(int index, int element){

	for (int i=arraySize; i>index; i--){
		array[i]=array[i-1];
	}
	array[index]=element;
	arraySize++;
}
void Array::removeIndex(int index){
	int temp;
	for (int i=index+1;i <=arraySize; i++) {
		temp=array[i];
		array[i-1]=temp;
	}
	arraySize--;
}
int Array::getMax(){
	int max=array[0];
	for(int i=0;i<arraySize;i++){
		if(array[i]>max){
			max=array[i];
		}
	}
	return max;
}
int Array::getMin(){
	int min=array[0];
	for(int i=0;i<arraySize;i++){
		if(array[i]<min){
			min=array[i];
		}
	}
	return min;
}
