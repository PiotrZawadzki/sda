/*
 * Trapeze.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Trapeze.h"
#include <string>
#include <iostream>

Trapeze::Trapeze(double podstawa1, double podstawa2, double wysokosc,
		std::string nameTrapeze) {
	name = nameTrapeze;
	a = podstawa1;
	h = wysokosc;
	b = podstawa2;

}
void Trapeze::describe() {
	std::cout << "Czworokat majacy przynajmniej jedna pare rownoleglych bokow."
			<< std::endl;

}
std::string Trapeze::getName() {

	return name;
}
void Trapeze::getArea() {
	std::cout << "Pole trapezu o podstawie a rownej " << a
			<< " i podstawie b rownej " << b << " oraz wysokosci " << h
			<< " to: " << ((a + b) / 2) * h << std::endl;
}

