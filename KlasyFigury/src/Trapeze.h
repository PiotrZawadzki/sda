/*
 * Trapeze.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TRAPEZE_H_
#define TRAPEZE_H_

#include "Figura.h"

class Trapeze: public Figura {
public:
	Trapeze(double podstawa1=10, double podstawa2=5, double wysokosc=5, std::string nameTrapeze="Trapez");
	 std::string getName();
	void describe();
	void getArea();
	void pause();


private:
	double a;
	double b;
	double h;
	std::string name;

};

#endif /* TRAPEZE_H_ */
