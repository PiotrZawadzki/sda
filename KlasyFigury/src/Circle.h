/*
 * Circle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

#include "Figura.h"

class Circle: public Figura {
public:
	Circle(double promien = 23, std::string nameCircle = "Kolo");
	std::string getName();
	void describe();
	void getArea();


private:
	double r;
	std::string name;

};

#endif /* CIRCLE_H_ */
