/*
 * Circle.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Circle.h"
#include <string>
#include <iostream>
Circle::Circle(double promien, std::string nameCircle) {
	name = nameCircle;
	r = promien;
}
void Circle::describe() {
	std::cout
			<< "Zbior wszystkich punktow plaszczyzny, ktorych odleglosc od ustalonego punktu na tej plaszczyznie,"
			<< std::endl;
	std::cout
			<< "nazywanego srodkiem kola, jest mniejsza lub rowna dlugosci promienia kola."
			<< std::endl;
}
std::string Circle::getName() {

	return name;
}
void Circle::getArea() {
	std::cout << "Pole kola o promieniu  " << r << " to: " << 3.14 * r * r
			<< std::endl;
}

