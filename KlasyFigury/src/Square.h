/*
 * Square.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef SQUARE_H_
#define SQUARE_H_
#include <string>
#include "Figura.h"

class Square: public Figura {
public:
	Square(double bok = 5, std::string nameSquare = "Kwadrat");
	std::string getName();
	void describe();
	void getArea();
	void draw();

private:
	double a;
	std::string name;

};

#endif /* SQUARE_H_ */
