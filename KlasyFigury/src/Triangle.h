/*
 * Triangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_
#include <string>
#include "Figura.h"

class Triangle: public Figura {
public:
	Triangle(double bok=10, double wysokosc=5, std::string nameTriangle="Trojkat");
	 std::string getName();
	void describe();
	void getArea();
	void pause();


private:
	double a;
	double h;
	std::string name;

};

#endif /* TRIANGLE_H_ */
