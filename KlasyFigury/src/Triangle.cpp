/*
 * Triangle.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include "Triangle.h"
#include <iostream>
#include <string>
Triangle::Triangle(double bok, double wysokosc, std::string nameTriangle) {
	name = nameTriangle;
	a = bok;
	h = wysokosc;
}

void Triangle::describe() {
	std::cout
			<< "Wielok�t o trzech bokach. Tr�jk�t to najmniejsza figura wypuk�a i domkni�ta,"
			<< std::endl;
}
void Triangle::getArea() {
	std::cout << "Pole trojkata o boku " << a << " i wysokosci " << h << " to: "
			<< (a * h) / 2 << std::endl;
}
std::string Triangle::getName() {

	return name;
}
void Triangle::pause() {
	std::cout << std::endl;
	std::cout << "*******************************************" << std::endl;
	std::cout << std::endl;
}

