//============================================================================
// Name        : KlasyFigury.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "Triangle.h"
#include "Figura.h"
#include "Square.h"
#include "Rectangle.h"
#include "Trapeze.h"
#include "Circle.h"
using namespace std;

int main() {
	Triangle trojkat(12, 7);
	Circle kolko(5);
	Rectangle prostokat(8, 14);
	Trapeze trapez(15, 12, 8);
	Square kwadrat(5);

	Figura* figureList[5] =
			{ &trojkat, &kolko, &prostokat, &trapez, &kwadrat };

	for (int i = 0; i < 5; i++) {
		std::cout << "Figura nr. " << i + 1 << std::endl;
		std::cout << figureList[i]->getName() << std::endl;
		figureList[i]->describe();
		figureList[i]->getArea();
		std::cout << std::endl;
	};
	kwadrat.draw();
	return 0;
}
