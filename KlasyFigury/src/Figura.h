/*
 * Figura.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef FIGURA_H_
#define FIGURA_H_
#include <string>
#include <iostream>
class Figura {

public:
	virtual std::string getName()=0;
	virtual void describe()=0;
	virtual void getArea()=0;


private:
	std::string name;

};

#endif /* FIGURA_H_ */
