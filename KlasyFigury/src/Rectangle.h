/*
 * Rectangle.h
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_
#include <string>
#include "Figura.h"

class Rectangle: public Figura {
public:
	Rectangle(double bok1=10, double bok2=5, std::string nameRectangle="Prostokat");
	std::string getName();
	void describe();
	void getArea();


private:
	double a;
	double b;
	std::string name;

};

#endif /* RECTANGLE_H_ */
