//============================================================================
// Name        : power.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int power(int numer, int potega) {

	if (potega == 0) {
		return 1;
	}
	int wynik=numer;
	for (int i=1; i<potega; i++) {
		wynik= wynik*numer;
	};
	return wynik;
}


int main() {
	std::cout<<power(3, 1);
	return 0;
}
