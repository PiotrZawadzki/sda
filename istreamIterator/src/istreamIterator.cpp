//============================================================================
// Name        : istreamIterator.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

// istream_iterator example
#include <iostream>     // std::cin, std::cout
#include <iterator>     // std::istream_iterator
#include <vector>
#include <algorithm>

int main () {
  double value1, value2;
  std::cout << "Please, insert two values: ";

  std::istream_iterator<double> eos;              // end-of-stream iterator
  std::istream_iterator<double> iit (std::cin);   // stdin iterator

  if (iit!=eos) value1=*iit;

  ++iit;
  if (iit!=eos) value2=*iit;

  std::cout << value1 << "*" << value2 << "=" << (value1*value2) << '\n';

  std::vector<int> myvector;
  for(int i = 0; i<10; i++){
	  myvector.push_back(i*10);};
  std::ostream_iterator<int> out_it(std::cout, ", ");
  std::copy (myvector.begin(), myvector.end(), out_it);

  std::vector<int> nextvector;
  for(int i= 0; i<10; i++){
	  nextvector.push_back(i);
  };
  std::vector<int>::iterator it=nextvector.begin();
  advance(it,7);
  for(; it!=nextvector.end();it++){
	  (*it)*=(*it);
  }
  std::cout<<std::endl;
  for(std::vector<int>::iterator itt=nextvector.begin();itt!=nextvector.end();itt++){
	  std::cout<<*itt<<" ";
  }
  std::cout<<std::endl;
  for(std::vector<int>::reverse_iterator rit = nextvector.rbegin(); rit!=nextvector.rend(); rit++){
	  std::cout<<*rit<<" ";
  }
  return 0;
}
