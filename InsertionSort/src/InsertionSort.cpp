//============================================================================
// Name        : InsertionSort.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int i, j;
	int array[9]={6,8,10,2,3,7,15,1,4};

	for(i = 1;i<9; i++){
		int temp = array[i];
		for (j= i-1;j>=0;j--){
			if(temp < array[j]){
				array[j+1]=array[j];
			} else
				break;
		}
		array[j+1] = temp;
	}
	for(i = 0; i<9; i++){
		std::cout<<array[i]<<" ,";
	}

	return 0;
}
