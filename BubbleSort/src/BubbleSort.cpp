//============================================================================
// Name        : BubbleSort.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	int i, j;
	int array[9] = { 6, 8, 10, 2, 3, 7, 15, 1, 4 };

	for(i = 0; i<8;i++){
		for(j = 8; j>=i+1;j--){
			if(array[j]<array[j-1]){
				int x = array[j];
				array[j]=array[j-1];
				array[j-1]=x;
			}
		}
	}
	for(i = 0; i<9; i++){
			std::cout<<array[i]<<" ,";
		}

	return 0;
}
