//============================================================================
// Name        : 03SitoEratostenesa.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
const int max_size = 50;
int main() {

	int t[max_size];

	for (int i=0;i<max_size;i++) {
		t[i]=i;
	}
	for (int i=2;i<max_size;i++) {
		if (t[i]!=0) {
			for (int j=2*i;j<max_size;j+=i) {
				t[j]=0;
			}
		}
	}
	cout<<"Liczby pierwsze z zakresu 50"<<endl;

	for (int i=2;i<max_size;i++) {
		if (t[i]!=0) {
			cout<<t[i]<<" "<<endl;
		}
	}

	return 0;
}
