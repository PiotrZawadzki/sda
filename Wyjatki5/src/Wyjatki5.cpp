//============================================================================
// Name        : Wyjatki5.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <exception>
using namespace std;

class myArray{
public:
	class memoryException : public exception {
	public:
		virtual const char* what() const throw(){
			return "memory esception";
		}
	};
	myArray(int size){
		*array = new int[size];
		array_size = size;
	}
	~myArray(){
		delete[] array;
	}
	int at(int x){
		return array[x];
	}
	void resize(int new_size){
		int *old_array = array;

		array = new int[new_size];
		for(int i =0; i<array_size;i++){
			array[i] = old_array[i];
		}
		delete[]  old_array;
		array_size = new_size;
	}
	void append(int element_to_add){
		int *temp = new int[array_size +1];
		for (int i = 0; i<array_size; i++){
			temp[i]= array[i];
		}
		temp[array_size +1]= element_to_add;
		array= temp;
		array_size +=1;

	}

private:
	int array_size;
	int *array[];
};
int main() {

	return 0;
}
