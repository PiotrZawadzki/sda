//============================================================================
// Name        : 03WskCwicz2.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

const int max_array_size = 5;
// wypisuje liczby parzyste
int main() {
    int t[max_array_size] = {0,6,8,9,10};
    cout << "Liczby parzyste w tablicy to: " << endl;
    int *it=t;
    for(int i=0; i<max_array_size; ++i){

        if ((*it)% 2 == 0) {
        cout << *it << endl;
        }
        it++;
    }
    return 0;
}
