//============================================================================
// Name        : 03NWDRekurencyjnie.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int nwd(int k, int n) {
	if (k == 0) {
		return n;
	} else if (k > 0) {
		cout << k << " " << n << endl;
		return nwd(n % k, k);
	}

}

int main() {
	cout << nwd(15, 55) << endl; // prints !!!Hello World!!!
	return 0;
}
