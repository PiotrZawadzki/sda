//============================================================================
// Name        : arytmetyka2.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	bool p = true;
	bool f = false;

	cout << "To wlasnie logika:" << endl;
	cout << "(p && f) --> " << (p && f) << endl;
	cout << "(p || f) --> " << (p || f) << endl;
	cout << "(p ^ f) --> " << (p ^ f) << endl;
	cout << "(p && p) --> " << (p && p) << endl;
	cout << "(p || p) --> " << (p || p) << endl;
	cout << "(p ^ p) --> " << (p ^ p) << endl;

	return 0;
}
