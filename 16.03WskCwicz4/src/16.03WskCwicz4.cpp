//============================================================================
// Name        : 03WskCwicz4.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
using namespace std;

const int max_array_size = 5;
// suma elementow tablicy wskazniki
int main() {
    int t[max_array_size] = {1,2,3,4,5};
    int sum = 0;
    int *it=t;
    for(int i=0; i<max_array_size; ++i){


        sum += (*it);
        it++;

    }
    cout << "Suma elementow tablicy wynosi = " << sum << endl;
    return 0;
}
