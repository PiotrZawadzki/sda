//============================================================================
// Name        : 03KlasyData.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "Data.h"
using namespace std;

int main() {
	Data dzisiaj(12, 5, 2005);

	dzisiaj.setDzien(12);
	dzisiaj.setMiesiac(3);
	dzisiaj.setRok(2001);
	//dzisiaj.addRok();
	//dzisiaj.addDzien();
	dzisiaj.isYear();
	dzisiaj.isDate();
	cout << endl;
	dzisiaj.getDate();

	dzisiaj.daysTillTheEndOfTheMonth();
	dzisiaj.daysTillTheEndOfTheYear();

	Data dzisiaj2(5, 10, 2005);
	dzisiaj2.getDate();
	return 0;
}
