/*
 * Data.h
 *
 *  Created on: 21.03.2017
 *      Author: RENT
 */

#ifndef DATA_H_
#define DATA_H_
class Data {
public:
	void setDzien(int day);
	void setMiesiac(int month);
	void setRok(int year);
	void addDni(int days);
	void addMiesiac(int months);
	void addLata(int years);
	void addRok();
	void addDzien();
	void getDate();
	void isYear();
	void isDate();
	void daysTillTheEndOfTheMonth();
	void daysTillTheEndOfTheYear();
	bool operator==(const Data& other) const;
	Data(int d, int m, int r);
private:
	int dzien;
	int miesiac;
	int rok;
	bool przestepny;
};

#endif /* DATA_H_ */
