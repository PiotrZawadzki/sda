#include "Data.h"
#include <iostream>
using namespace std;

void Data::setDzien(int day) {
	dzien = day;
}
void Data::setMiesiac(int month) {
	miesiac = month;
}
void Data::setRok(int year) {
	rok = year;
}
void Data::addLata(int years) {
	rok += years;
}
void Data::addRok() {
	rok++;
}
void Data::addDzien() {
	if (przestepny == true && miesiac == 2 && dzien == 29) {
		miesiac++;
		dzien = 1;
	} else if (przestepny == false && miesiac == 2 && dzien == 28) {
		miesiac++;
		dzien = 1;
	} else if ((miesiac == 4 || miesiac == 6 || miesiac == 9 || miesiac == 11)
			&& dzien == 30) {
		miesiac++;
		dzien = 1;
	} else if (miesiac == 12 && dzien == 31) {
		rok++;
		dzien = 1;
		miesiac = 12;
	} else if ((miesiac == 1 || miesiac == 3 || miesiac == 5 || miesiac == 7
			|| miesiac == 8 || miesiac == 10) && dzien == 31) {
		miesiac++;
		dzien = 1;
	}

}

void Data::isYear() {
	if (((rok % 4 == 0) && (rok % 100 != 0)) || (rok % 400 == 0)) {
		przestepny = true;

	} else {
		przestepny = false;
	}
}
void Data::isDate() {
	switch (miesiac) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12: {
		if (dzien <= 31 && dzien >= 1) {
			cout << " poprawna data";
		} else {
			cout << "niepoprawna data";
		}

		break;
	}
	case 4:
	case 6:
	case 9:
	case 11: {
		if (dzien <= 30 && dzien >= 1) {
			cout << " poprawna data";
		} else {
			cout << "niepoprawna data";
		}
		break;
	}
	case 2: {
		if (przestepny && dzien <= 29 && dzien >= 1) {
			cout << " poprawna data";
		} else if (przestepny == false && dzien <= 28 && dzien >= 1) {
			cout << " poprawna data";
		} else {
			cout << "niepoprawna data";
		}
		break;
	}
	}
}
void Data::getDate() {
	std::cout << "Podana data to: dzien " << dzien << " miesiac " << miesiac
			<< " rok " << rok << std::endl;
}
void Data::daysTillTheEndOfTheMonth() {
	int liczbaDni;
	if (przestepny == true && miesiac == 2) {
		liczbaDni = 29 - dzien;
		cout << "Do konca miesiaca pozostalo " << liczbaDni << " dni" << endl;
	} else if (przestepny == false && miesiac == 2) {
		liczbaDni = 28 - dzien;
		cout << "Do konca miesiaca pozostalo " << liczbaDni << " dni" << endl;
	} else if (miesiac == 4 || miesiac == 6 || miesiac == 9 || miesiac == 11) {
		liczbaDni = 30 - dzien;
		cout << "Do konca miesiaca pozostalo " << liczbaDni << " dni" << endl;
	} else {
		liczbaDni = 31 - dzien;
		cout << "Do konca miesiaca pozostalo " << liczbaDni << " dni" << endl;
	}
}
void Data::daysTillTheEndOfTheYear() {
	int dni = 0;
	int przestepneDniMies[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int niePrzestepneDniMies[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,
			31 };
	if (przestepny == true) {
		for (int i = miesiac; i < 12; i++) {
			dni += przestepneDniMies[i];
		}
		dni += przestepneDniMies[miesiac - 1] - dzien;
		cout << "Do ko�ca roku zosta�o " << dni << endl;
	} else {
		for (int i = miesiac; i < 12; i++) {
			dni += niePrzestepneDniMies[i];
		}
		dni += niePrzestepneDniMies[miesiac - 1] - dzien;
		cout << "Do ko�ca roku zosta�o " << dni << endl;
	}
}
Data::Data(int d, int m, int r) {
	dzien = d;
	miesiac = m;
	rok = r;

}
