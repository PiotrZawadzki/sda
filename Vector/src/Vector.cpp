//============================================================================
// Name        : Vector.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
// constructing vectors
#include <iostream>
#include <vector>

int main ()
{
  // constructors used in the same order as described above:
  std::vector<int> first;                                // empty vector of ints
  std::vector<int> second (4,100);                       // four ints with value 100
  std::vector<int> third (second.begin(),second.end());  // iterating through second
  std::vector<int> fourth (third);                       // a copy of third

  // the iterator constructor can also be used to construct from arrays:
  int myints[] = {16,2,77,29};
  std::vector<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );

  std::cout << "The contents of fifth are:";
  for (std::vector<int>::iterator it = fifth.begin(); it != fifth.end(); ++it)
    std::cout << ' ' << *it;
  std::cout << '\n';

  std::cout << "The contents of second are:";
    for (std::vector<int>::iterator it = second.begin(); it != second.end(); ++it)
      std::cout << ' ' << *it;
    std::cout << '\n';
    std::cout << "The contents of third are:";
      for (std::vector<int>::iterator it = third.begin(); it != third.end(); ++it)
        std::cout << ' ' << *it;
      std::cout << '\n';

    std::vector<double> rozmiardouble;
    for (int i=0; i<100; i++) rozmiardouble.push_back(i);

     std::cout << "size: " << (int) rozmiardouble.size() << '\n';
     std::cout << "capacity: " << (int) rozmiardouble.capacity() << '\n';
     std::cout << "max_size: " << (int) rozmiardouble.max_size() << '\n';

     std::vector<int> myvector;
       int myint;

       std::cout << "Please enter some integers (enter 0 to end):\n";

       do {
         std::cin >> myint;
         myvector.push_back (myint);
         std::cout << "capacity: " << (int) myvector.capacity() << '\n';
       } while (myint);

       myvector.erase(myvector.begin(), myvector.begin()+2);

       std::cout << "myvector stores " << int(myvector.size()) << " numbers.\n";


       std::vector<int> vectorek (20);
       for(int i = 0; i<vectorek.size();i++){
    	   vectorek[i]=i;
       }
       std::cout<<"Vectorek zawiera: \n";
       for(int i = 0; i<vectorek.size();i++){
    	  std::cout<< vectorek.at(i)<<std::endl;
       }


  return 0;
}
