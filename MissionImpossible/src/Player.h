/*
 * Player.h
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "GameObject.h"

class Player: public GameObject {
public:
	Player();
	virtual ~Player();
	char getSymbol();
};

#endif /* PLAYER_H_ */
