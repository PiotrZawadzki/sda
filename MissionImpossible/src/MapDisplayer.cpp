/*
 * MapDisplayer.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "MapDisplayer.h"
#include <iostream>

MapDisplayer::MapDisplayer() {
	map = nullptr;

}

MapDisplayer::~MapDisplayer() {
	// TODO Auto-generated destructor stub
}

void MapDisplayer::setMap(Map* _map) {
	map = _map;
}

void MapDisplayer::displayMap() {
	if (map == nullptr){
		return;
	}
	for(int i=0;i<25;i++){
		for(int j=0;j<25;j++){
			std::cout<<map->getPoint(j, i);
		}
		std::cout<<std::endl;
	}
}
