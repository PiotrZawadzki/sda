//============================================================================
// Name        : MissionImpossible.cpp
// Author      : a
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "KeyManager.h"
#include "Map.h"
#include "MapDisplayer.h"
#include "GameLogic.h"
#include "GameObject.h"
using namespace std;

void printHelp() {
	cout << "Help: " << endl << "o - exit" << endl << "r - idle" << endl
			<< "w - up" << endl << "s - down" << endl << "a - left" << endl
			<< "d - right" << endl;
}

int main() {

	bool exit = false;
	KeyManager *key_manager = new KeyManager;    //utowrzenie obiektu KeyManager
	GameLogic *game_logic = new GameLogic;   		//utowrzenie obiektu Logiki
	Map *map = new Map;                                //utworzenie obiektu mapy
	MapDisplayer *map_displayer = new MapDisplayer; //utworzenie obiektu wyswietlania mape
	map_displayer->setMap(map);
	map->setObjectList(game_logic->getObjectList()); //przekazanie mapy do mapdisplayera
	game_logic->setKeyManager(key_manager);

	game_logic->configureGame();
	map->generateMap();              //generowanie mapy
	map_displayer->displayMap();

	while (exit == false) {								 //petla glowna gry
			//sprawdzic czy gra jest skonfigurowana
		char key;
		cout << "Please enter key and press enter.(h for help)"; //popros gracza o kolejny znak
		cin >> key;                                      //wczytamy kolejny znak
		if (key == 'h') {
			printHelp();
			continue;
		}

		key_manager->addNewKey(key);
		if(game_logic->nextTurn()){//Logika gry
			map->generateMap();              //generowanie mapy
			map_displayer->displayMap();
			exit = true;
			std::cout<<"GOTCHA! GAME OVER!"<<std::endl;
			continue;
		}
		map->generateMap();              //generowanie mapy
		map_displayer->displayMap();      //wyswietlanie mapy

	}
	delete key_manager;         //usuniecie obiektu KeyManager
	delete game_logic;          //usuniecie obiektu Logiki
	delete map;					//usuniecie obiektu mapy
	delete map_displayer;		//usuniecie obiektu wyswietlania mape
	cout << "Game ended successfull";

	return 0;
}
